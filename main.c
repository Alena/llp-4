#include "linked_list.h"
#include "ho_functions.h"
#include <stdio.h>
#include <stddef.h>
#include <limits.h>
#include <math.h>


int abs(int a);

/*
void print_output(l_list* array, char *str) {
	printf(str); 
	print_list(array);
	printf("Размер листа: %d\n", list_length(array)); 
	printf("Сумма элементов: %d\n",  list_sum(array)); 
}

void print_index(l_list* array) {
	printf("Введите индекс\n");
	int index;
	scanf("%d", &index);
	printf("Элемент (%d) равен: ", index );
	printf("%d\n", list_get(array, index ));
}
*/
static void print_space(int x){
	printf("%d ",x);
}

static void print_newline(int x){
	printf("\n%d",x);
}

static int pow_square(int x){
	if (x*x>INT_MAX) 
		return 0;
	return x*x;
}

static int pow_cube(int x){
	if (x*x*x>INT_MAX || x*x*x<INT_MIN) 
		return 0;
	return x*x*x;
}

static int sum(int a,int b){
	if ((a+b)>INT_MAX || (a+b)<INT_MIN) 
		return 0;
	return a+b;
}

static int max_value(int a,int b){
	if (a>=b) 
		return a;
	return b;
}

static int min_value(int a,int b){
	if (a<=b) 
		return a;
	return b;
}

static int power_of_two(int a){
	if (a*2>INT_MAX || a*2<INT_MIN) 
		return 0;
	return a*2;
}

int main (void) {
	l_list* array;
	l_list* new_pointer;

	array = create_list();

	printf("Через пробел\n");
	foreach(array,print_space);

	printf("\nЧерез перенос строки:");
	foreach(array,print_newline);


	new_pointer = map(pow_square,array);
	printf("\n\tКвадраты\n");
	foreach(new_pointer,print_space);
	list_free(new_pointer);

	printf("\n\tКубы\n");
	new_pointer = map(pow_cube,array);
	foreach(new_pointer,print_space);
	list_free(new_pointer);


	printf("\n\nСумма элементов = %d\n",foldl(0,sum,array));
	printf("Максимальный элемент = %d\n",foldl(INT_MIN,max_value,array));
	printf("Минимальный элемент = %d\n",foldl(INT_MAX,min_value,array));


	map_mut(abs,array);
	printf("\n\tМодули\n");
	foreach(array,print_space);


	new_pointer = iterate(2,10,power_of_two);
	printf("\n\tПервые 10 значений 2^x \n");
	foreach(new_pointer,print_space);
	list_free(new_pointer);



	if(!save(array, "list.txt")){
		printf("\nОшибка при сохранении\n");
		return 0;
	}else
    		printf("\nLinked list успешно сохранен\n");



    	new_pointer = load("list.txt");
    	if (new_pointer != NULL){
    		printf("Файл загружен\n");
    		foreach(new_pointer,print_space);
    	}else {
		printf("Ошибка при чтении\n");
    		return 0;
	}



    	if (!serialize(array, "list.bin")) {
		printf("\nОшибка при сериализации\n");
           	return 0;
	}else
    		printf("\nСериализация закончена\n");



	list_free(new_pointer);
	new_pointer = deserialize("list.bin");
   	if (new_pointer != NULL){
    		printf("Десериализвция закончена\n");
    		foreach(new_pointer,print_space);
    	}else {
    		printf("Ошибка при десериализвция\n");
    		return 0;
	}

	printf("\n");
	list_free(new_pointer);
	list_free(array);
	return 0;

}



