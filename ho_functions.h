#ifndef HO_FUNCTIONS
#define HO_FUNCTIONS
void foreach(l_list* start,void (*func)(int));
l_list* map(int (*func)(int), l_list* start);
void map_mut(int (*func)(int), l_list* start);
int foldl(int rax,int (*func)(int,int),l_list* start);
l_list* iterate(int s,int n,int (*func)(int));
#endif
